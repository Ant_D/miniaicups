from itertools import product

import argparse
import sys
import time

import asyncio
from asyncio import events

from mechanic.game import Game
from mechanic.strategy import KeyboardClient, FileClient


def parse_args():
    parser = argparse.ArgumentParser(description='LocalRunner for MadCars')
    parser.add_argument('-f', '--fp', type=str, nargs='?',
                        help='Path to executable with strategy for first player', default='keyboard')
    parser.add_argument('--fpl', type=str, nargs='?', help='Path to log for first player')

    parser.add_argument('-s', '--sp', type=str, nargs='?',
                        help='Path to executable with strategy for second player', default='keyboard')
    parser.add_argument('--spl', type=str, nargs='?', help='Path to log for second player')
    parser.add_argument('-v', '--visual', nargs='?',help='Is localrunner used for lea', const=True, default=False)

    #maps = ['PillMap', 'PillHubbleMap', 'PillHillMap', 'PillCarcassMap', 'IslandMap', 'IslandHoleMap']
    #cars = ['Buggy', 'Bus', 'SquareWheelsBuggy']
    #games = [','.join(t) for t in product(maps, cars)]
    games = ["PillMap,Buggy,3"]

    parser.add_argument('-m', '--matches', nargs='+', help='List of pairs(map, car, times) for games', default=games)
    parser.add_argument('-S', '--scale', type=float, help='Window scale', default=1)

    args = parser.parse_args()
    return args


def make_matches(raw_matches):    
    matches = []
    for x in raw_matches:
        match, _, times = x.rpartition(",")
        times = int(times)
        matches.extend([match] * times)
    return matches


def visual_run(args):
    import pyglet
    import pymunk.pyglet_util
    scale = args.scale

    window = pyglet.window.Window(1200 * scale, 800 * scale, vsync=False)
    draw_options = pymunk.pyglet_util.DrawOptions()
    _ = pyglet.clock.ClockDisplay(interval=0.016)
    pyglet.gl.glScalef(scale, scale, scale)

    if args.fp == 'keyboard':
        fc = KeyboardClient(window)
    else:
        fc = FileClient(args.fp.split(), args.fpl)

    if args.sp == 'keyboard':
        sc = KeyboardClient(window)
    else:
        sc = FileClient(args.sp.split(), args.spl)

    matches = make_matches(args.matches)       

    game = Game([fc, sc], matches, extended_save=False)

    loop = events.new_event_loop()
    events.set_event_loop(loop)

    @window.event
    def on_draw():
        pyglet.gl.glClearColor(255,255,255,255)
        window.clear()
        game.draw(draw_options)
        if not game.game_complete:
            future_message = loop.run_until_complete(game.tick())
        else:
            winner = game.get_winner()
            if winner:
                pyglet.text.Label("Player {} win".format(winner.id), 
                                font_name='Times New Roman',
                                font_size=36,
                                color=(255, 0, 0, 255),
                                x=600, y=500,
                                anchor_x='center', anchor_y='center').draw()
            else:
                pyglet.text.Label("Draw", font_name='Times New Roman',
                                  font_size=36,
                                  color=(255, 0, 0, 255),
                                  x=600, y=500,
                                  anchor_x='center', anchor_y='center').draw()
    pyglet.app.run()


def winner_win_ratio(game):
    winner = game.get_winner()
    max_lives = game.max_match_count
    total_matches = 2 * max_lives - winner.lives
    win_ratio = max_lives / total_matches
    return win_ratio
  

def console_run(args):
    fc = FileClient(args.fp.split(), args.fpl)
    sc = FileClient(args.sp.split(), args.spl)
    matches = make_matches(args.matches)       
    game = Game([fc, sc], matches, extended_save=False)
    loop = events.new_event_loop()
    events.set_event_loop(loop)
    while True:
        if not game.game_complete:
            future_message = loop.run_until_complete(game.tick())
        else:
            winner = game.get_winner()
            win_ratio = winner_win_ratio(game)
            print(winner.id, win_ratio, file=sys.stderr)
            for player in game.all_players:
                player.client.wait_finished()
            break


args = parse_args()
if args.visual:
    visual_run(args)   
else:
    console_run(args)
